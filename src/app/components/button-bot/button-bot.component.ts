import { BotService } from './../../service/bot.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-bot',
  templateUrl: './button-bot.component.html',
  styleUrls: ['./button-bot.component.css']
})
export class ButtonBotComponent implements OnInit {
  public botService : BotService;
  constructor(_botService : BotService) { 
    this.botService = _botService;
  }

  ngOnInit() {
  }

}
