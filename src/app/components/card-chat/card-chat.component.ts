import { element } from 'protractor';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DialogflowService } from '../../service/dialogflow.service';
import { BotService } from 'src/app/service/bot.service';
import { Mensagem } from 'src/app/model/mensagem.model';

@Component({
  selector: 'app-card-chat',
  templateUrl: './card-chat.component.html',
  styleUrls: ['./card-chat.component.css']
})
export class CardChatComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  public msg: string;
  public resultados: Array<Mensagem>;
  public suggestions: Array<any>;

  constructor(private dialogflowService: DialogflowService, public botService : BotService) { 
    this.initBoot()
  }

  ngOnInit() {
  }

  //ngx-markdown
  options = {
    gfm: true,
    tables: false, // changed
    breaks: true,
    pedantic: false,
    sanitize: false,
    smartLists: true,
    smartypants: false
  };
  interpolate = {
    language: 'I speak english'
  }
  callback = (error: any, result: string) => {
    console.log(`callback`, error, result);
  }

  private initBoot() {
    this.resultados = []
    this.dialogflowService.getResponse('oi')
      .subscribe((lista: any) => {
        lista.result.fulfillment.messages.forEach((element) => {
          if(element.platform === 'google'){
            if(element.type === 'simple_response'){
              this.resultados.push(new Mensagem('boot', element.textToSpeech, lista.timestamp, 'text'));
            }
            if(element.type === 'suggestion_chips'){
              this.suggestions = element.suggestions;
            }
          }else{
            this.resultados.push(new Mensagem('boot', element.speech, lista.timestamp, 'text'));
          }
        });
      })
  }
  public sendSuggestion(suggestion: any) {
    this.msg = suggestion.title;
    this.sendMessage();
  }
  public sendMessage() {
    this.suggestions = [];
    this.resultados.push(new Mensagem('eu', this.msg, new Date(), 'text'));
    this.dialogflowService.getResponse(this.removerAcentos(this.msg))
      .subscribe((lista: any) => {
        lista.result.fulfillment.messages.forEach((element) => {
          console.log(element);
          if(element.platform === 'google'){
            if(element.type === 'simple_response'){
              this.resultados.push(new Mensagem('boot', element.textToSpeech, lista.timestamp, 'text'));
            }
            if(element.type === 'suggestion_chips'){
              this.suggestions = element.suggestions;
            }
          }else{
            if(element.type === 0) {
              this.resultados.push(new Mensagem('boot', element.speech, lista.timestamp, 'text'));
            }
          }
        });
      }).add(() =>{
        this.playNotification("resposta");
      });
    this.msg = '';
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  private removerAcentos(s: string) {
    return s.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
  }

  private playNotification(tipo : string){
    if(tipo === 'resposta'){
      var audio = new Audio('../../../assets/sound/notify-response.mp3');
      audio.play();
    }
  }
}
