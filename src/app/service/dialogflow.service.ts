import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DialogflowService {
  
  private baseURL: string = "https://api.dialogflow.com/v1/query?v=20150910";
  //private baseURL: string = "https://dialogflow.googleapis.com/$discovery/rest?version=v2";
  private token: string = 'ed02409805bd4db1b0de1904736e038b'

  constructor(private http: HttpClient) { }

  public getResponse(query: string) {
    let data = {
      query: query,
      lang: 'en',
      sessionId: '12345'
    }
    return this.http
      .post(`${this.baseURL}`, data, { headers: this.getHeaders() })


  }

  public getHeaders() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.token}`);
    return headers;
  }
}
