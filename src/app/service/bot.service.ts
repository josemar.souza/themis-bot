import { Injectable } from '@angular/core';
import { Agente } from '../model/agente.model';

@Injectable({
    providedIn: 'root'
})
export class BotService {
    public agente : Agente = new Agente("Themis","Online");
    public showButton = true;
    public showChat = false;

    public showChatEvent() {
        this.showButton = false;
        this.showChat = true;
    }

    public closeChatEvent(){
        this.showButton = true;
        this.showChat = false;
    }
}