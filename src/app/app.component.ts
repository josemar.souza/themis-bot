import { Component } from '@angular/core';
import { BotService } from './service/bot.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public botService : BotService;
  public title = 'themis-bot';
  
  constructor(_botService: BotService) { 
    this.botService = _botService;
  }
}
