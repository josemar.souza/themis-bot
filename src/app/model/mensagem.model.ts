export class Mensagem {
    constructor(
        public remetente?: string,
        public mensagem?: string,
        public data?: Date,
        public tipo? : string){    }
}